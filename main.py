from pyzbar import pyzbar
import cv2
import pymysql
import time

connection = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='barkod', cursorclass=pymysql.cursors.DictCursor)
read = "2"
def barcodeChack(barcodeNumber):
	curr = connection.cursor()
	sql = "SELECT * FROM `depo` WHERE `barcode_number`=%s"
	curr.execute(sql, (barcodeNumber))
	result = curr.fetchall()
	return result
def addProduct(barcodeNumber):
	curr = connection.cursor()
	product_name = input("Ürün adı :")
	product_count = input("Ürün adeti :")
	if product_name != "" and product_count != "":
		sql = "INSERT INTO depo(barcode_number, product_name, product_count) VALUES (%s,%s,%s)"
		curr.execute(sql, (barcodeNumber, product_name, product_count))
		connection.commit()
		read = "1"

def updateProductCount(barcodeNumber):
	check = len(barcodeChack(barcodeNumber))
	if check > 0:
		getData = barcodeChack(barcodeNumber)
		for get in getData:
			product_count = get["product_count"]
		curr = connection.cursor()
		sql = "UPDATE depo SET product_count = %s WHERE barcode_number = %s"
		curr.execute(sql, (int(product_count+1), barcodeNumber ))
		connection.commit()
		read = "1"

cam = cv2.VideoCapture(0)
while True:
	ret, img = cam.read()
	barcodes = pyzbar.decode(img)
	for barcode in barcodes:
		(x, y, w, h) = barcode.rect
		cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
		barcodeData = barcode.data.decode("utf-8")
		read = input("Okutmak için 2 iptal etmek için 1 : ")
		if read == "2":
			print("Okunan Barkod : {} {}".format(barcodeData,type(int(barcodeData))))
			if len(barcodeChack(barcodeData)) > 0:
				updateProductCount(barcodeData)
			else:
				addProduct(barcodeData)
	cv2.imshow('Kamera', img)
	if cv2.waitKey(25) & 0xFF == ord('q'):
		break
cam.realese()
cv2.destroyAllWindows()